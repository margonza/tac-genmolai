# Model to generate ligands for a4a5b2
### Important references:
- [Main paper](https://doi.org/10.1021/acs.molpharmaceut.9b00634) describes a shape generating network and a captioning network.
From the main paper:
  - Shape generating network: [Bicycle GAN](https://arxiv.org/abs/1711.11586) original pytorch code can be found [here](https://github.com/junyanz/BicycleGAN).
  - Captioning network: an Encoder and DecoderRNN trained with a VAE. First described in this [paper](https://pubs.acs.org/doi/10.1021/acs.jcim.8b00706). The code can be found [here](https://github.com/compsciencelab/ligdream) but it is outdated!
- [Libmolgrid](https://pubs.acs.org/doi/10.1021/acs.jcim.9b01145): python module to voxelize proteins. The code can be found [here](https://github.com/gnina/libmolgrid) and the documentation [here](https://gnina.github.io/libmolgrid/).

### Shape network
#### Bicycle GAN

This model is used to produce a distribution of ligand shapes for a protein pocket. The architecture (taken from original paper, all credits to the authors) is shown in the figure below:

![Figure1](Image/img1.png)
"Figure 1: Overview: (a) Test time usage of all the methods. To produce a sample output, a latent code z is first randomly sampled from a known distribution (e.g., a standard normal distribution). A generator G maps an input image A (blue) and the latent sample z to produce a output sample Bˆ (yellow). (b) pix2pix+noise baseline, with an additional ground truth image B (brown) that corresponds to A. (c) cVAE-GAN starts from a ground truth target image B and encode it into the latent space. The generator then attempts to map the input image A along with a sampled z back into the original image B. (d) cLR-GAN randomly samples a latent code from a known distribution, uses it to map A into the output Bˆ, and then tries to reconstruct the latent code from the output. (e) Our hybrid BicycleGAN method combines constraints in both directions."

The idea is to train both the encoder and generator to learn to map in both directions: 

- cVAE-GAN
  real output B --> encoded latent code z --> generated output B^
- cLR-GAN
  random latent code z --> generated output B^ --> encoded latent code z

This will encourage bijection between the output and latent space and reduce mode collapse:
- The latent space can’t map to many outputs.
- Every output should be mapped back into a single latent space.

##### Loss functions:
The encoder and generator are trained both as cVAE-GAN and cLR-GAN. However, there is a D_cVAE and a D_cLR discriminator which are trained independently (sort of).

cVAE-GAN
- Discriminator D_cVAE:
  - Mean squared error (MSE) loss function.
- Encoder and Generator:
  - Updated via the discriminators with MSE loss, to see if they are able to fool each Discriminator.
  - KL divergence: to make sure the Encoder produces a distribution.
  - L1 loss: Reconstruction of ground truth (protein with real ligand shapes vs protein with generated ligand shape) (|G(A, z) - B|) 

cLR-GAN
- Discriminator D_cLR:
  - mean squared error (MSE) loss function.
- Encoder and Generator: 
  - Both encoder and generator are updated via the discriminators with MSE loss, to see if they are able to fool the discriminator.
- Generator:
   - L1 loss, reconstruction of random z vs encoded z

#### How was the shape network implemented? (for now, it can be improved)

The code for the networks was provided as additional material in the paper and is writen in pytorch. I wrote the training and dataloader in pytorch, following the original bicycleGANS code and the reference paper. The reference paper does not state the batch size they used, for now I am using batch size 1 instead of two as in the bicycleGAN paper. I would like to discuss this tho...

##### Data
Initially, all ligands taken from [DUD-E](http://dude.docking.org) were docked using [SMINA](https://www.cheminformania.com/ligand-docking-with-smina/). All docked ligands and proteins were put in the same directory. 

After comparing the protein pockets of the proteins in DUDE and the nicotinic receptor a4a5b2, I noticed there were not many similarities so I expended the database. I will also included proteins that could bind known nicotinic ligands (as determined by swiss target prediction).
##### Libmolgrid
As dataloader and to voxelize the proteins with their ligands I am using Libmolgrid. I indicate the path were all the proteins and ligands are and provide Libmolgrid with a file.types with the following columns:

group | affinity | protein.pdb | ligand.pdb

The data loader will take the pdb in the last column (the ligand) as reference to center the grid. Before being voxelized, each protein and ligand-ligand are randomly rotated and transposed. After being voxelized we get a tensor with 28 channels the first 14 channels corresponf to the protein and the next 14 channels to the ligand. The channels have this information:

Protein 14 channels (molgrid.defaultGninaReceptorTyper.get_type_names()):
AliphaticCarbonXSHydrophobe, AliphaticCarbonXSNonHydrophobe, AromaticCarbonXSHydrophobe, AromaticCarbonXSNonHydrophobe, Bromine_Iodine_Chlorine_Fluorine, Nitrogen_NitrogenXSAcceptor, NitrogenXSDonor_NitrogenXSDonorAcceptor, Oxygen_OxygenXSAcceptor, OxygenXSDonorAcceptor_OxygenXSDonor, Sulfur_SulfurAcceptor, Phosphorus, Calcium, Zinc, GenericMetal_Boron_Manganese_Magnesium_Iron

Ligand 14 channels (molgrid.defaultGninaLigandTyper.get_type_names()):
AliphaticCarbonXSHydrophobe, AliphaticCarbonXSNonHydrophobe, AromaticCarbonXSHydrophobe, AromaticCarbonXSNonHydrophobe, Bromine_Iodine, Chlorine, Fluorine, Nitrogen_NitrogenXSAcceptor, NitrogenXSDonor_NitrogenXSDonorAcceptor, Oxygen_OxygenXSAcceptor, OxygenXSDonorAcceptor_OxygenXSDonor, Sulfur_SulfurAcceptor, Phosphorus, GenericMetal_Boron_Manganese_Magnesium_Zinc_Calcium_Iron

The grid has a resolution of 0.5 and dimension along each side of the cube of 23.5.  The atom density values are constrained to a Gaussian, to a multiple of the atomic radius (grm), then decaying to 0 quadratically at 1+2grm^2 / 2grm. All of these are default values.

### Captioning network
#### VAE - Encoder - DecoderRNN

This model is used to produce SMILE strings from ligand shapes. The architecture (taken from original paper, all credits to the authors) is shown in the figure below:

![Figure2](Image/img2.png)
![Figure3](Image/img3.png)
"Figure 2. Proposed compound generating pipeline consisting of (top) a shape autoencoder and (bottom) a shape captioning network."

The shape auntoencoder is a 3D VAE and is used to generate shapes of the ligand that are imperfect. It is important to train this VAE with the captioning network, this will ensure that the captioning network will still be able to translate the "imperfect" generated ligand shapes into SMILES. Otherwise, we would be training the captioning network only with "perfectly" voxelized ligands. 

The second network is and Encoder that uses CNNs to encode the shapes into a vector that is fed into the Decoder, which is a long short-term memory (LSTM) network that will decode the vector into SMILES.

#### How was the model implemented (for now, again, it can be improved)
All code was provided in the ligdream paper but it is outdated. In addition they used moleculekit to voxelize the proteins. The up-to-date version of Moleculekit requires a licence, so I rewrote all the code to use Libmolgrid instead. The curated data was provided and I am working with it.

##### Data
ZINC15 database:
- drug-like subset (logP ≤ 5 and molecular weight 250−500 Da).
- SMILES strings of length inferior to 60 characters 
- radicals were removed

To train the networks I use canonical SMILES. A 3D representation of the SMILES is obtained with openbabel MMFF94 force field and then voxelized with Libmolgrid.
